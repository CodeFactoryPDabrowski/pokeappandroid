import com.codefactory.pokeapp.gradle.dependency.Dependencies.Coroutines
import com.codefactory.pokeapp.gradle.extension.bulkTestImplementation

plugins {
    id("com.android.library")
    id("module-plugin")
    id("di-plugin")
}

dependencies {
    implementation(project(":network"))
    implementation(project(":storage"))
    implementation(project(":shared"))
    implementation(project(":sharedunittest"))
    bulkTestImplementation(Coroutines.Test())
}