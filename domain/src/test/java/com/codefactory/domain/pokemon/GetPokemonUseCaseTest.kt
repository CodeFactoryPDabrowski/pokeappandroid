package com.codefactory.domain.pokemon

import app.cash.turbine.test
import com.codefactory.domain.pokemon.mapper.DbToDomainMapper
import com.codefactory.pokeapp.sharedunittest.CoroutineTestRule
import com.codefactory.pokeapp.sharedunittest.runBlockingTest
import com.codefactory.shared.domain.Result
import com.codefactory.shared.domain.data
import com.codefactory.storage.pokemon.PokemonStorageSource
import com.codefactory.storage.pokemon.entity.PokemonDb
import com.codefactory.storage.pokemon.entity.SpritesDb
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.lang.RuntimeException
import kotlin.time.ExperimentalTime

@ExperimentalTime
@RunWith(MockitoJUnitRunner::class)
class GetPokemonUseCaseTest {

    @get:Rule
    var coroutineRule = CoroutineTestRule()

    @Mock
    private lateinit var pokemonStorageSource: PokemonStorageSource

    private val dbToDomainMapper = DbToDomainMapper()

    private lateinit var getPokemonUseCase: GetPokemonUseCase

    @Before
    fun setup() {
        getPokemonUseCase =
            GetPokemonUseCase(pokemonStorageSource, dbToDomainMapper, coroutineRule.testDispatcher)
    }

    @Test
    fun `When use case executed then expected success`() = coroutineRule.runBlockingTest {
        val pokemonId = 1
        val sprites = SpritesDb(
            "frontDefault", null,
            null, null,
            null, null, null, null
        )
        val pokemonDb = PokemonDb(pokemonId, "name", sprites)
        //WHEN
        whenever(pokemonStorageSource.get(pokemonId)).thenReturn(flowOf(pokemonDb))

        //GIVEN
        val test = getPokemonUseCase(GetPokemonUseCase.Params(pokemonId))

        //VERIFY
        test.test {
            val result = expectItem()
            assert(result is Result.Success)
            Assert.assertEquals(pokemonDb.name, result.data?.name)
            Assert.assertEquals(sprites.frontDefault, result.data?.frontImageUrl)

            expectComplete()
        }
    }

    @Test
    fun `When use case executed then expected error`() = coroutineRule.runBlockingTest {
        val pokemonId = 1
        val errorMessage = "Cannot get pokemon"
        val error = RuntimeException(errorMessage)
        //WHEN
        whenever(pokemonStorageSource.get(pokemonId)).thenReturn(flow { throw error })

        //GIVEN
        val test = getPokemonUseCase(GetPokemonUseCase.Params(pokemonId))

        //VERIFY
        test.test {
            val result = expectItem()
            assert(result is Result.Error)
            assert((result as Result.Error).exception.message?.contains(errorMessage) == true)

            expectComplete()
        }
    }

}