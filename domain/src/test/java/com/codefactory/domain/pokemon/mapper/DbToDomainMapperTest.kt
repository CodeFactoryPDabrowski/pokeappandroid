package com.codefactory.domain.pokemon.mapper

import com.codefactory.storage.pokemon.entity.PokemonDb
import com.codefactory.storage.pokemon.entity.SpritesDb
import org.junit.Assert.assertEquals
import org.junit.Test

class DbToDomainMapperTest {

    private val dbToDomainMapper = DbToDomainMapper()

    @Test
    fun `When db object provided then map it to domain object`() {
        //WHEN
        val sprites = SpritesDb(
            "frontDefault", null,
            null, null,
            null, null, null, null
        )
        val dbObject = PokemonDb(1, "DbName", sprites)

        //GIVEN
        val test = dbToDomainMapper.transform(dbObject)

        //VERIFY
        assertEquals(dbObject.id, test.id)
        assertEquals(dbObject.name, test.name)
        assertEquals(dbObject.sprites.frontDefault, test.frontImageUrl)
    }
}