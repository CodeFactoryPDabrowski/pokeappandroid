package com.codefactory.domain.pokemon

import com.codefactory.network.pokemon.PokemonNetworkSource
import com.codefactory.network.pokemon.entity.PokemonJson
import com.codefactory.network.pokemon.entity.PokemonUrlJson
import com.codefactory.network.pokemon.entity.SpriteJson
import com.codefactory.pokeapp.sharedunittest.CoroutineTestRule
import com.codefactory.pokeapp.sharedunittest.runBlockingTest
import com.codefactory.shared.domain.Result
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FetchPokemonPageUseCaseTest {

    @get:Rule
    var coroutineRule = CoroutineTestRule()

    @Mock
    private lateinit var pokemonNetworkSource: PokemonNetworkSource

    private lateinit var fetchPokemonsPageUseCase: FetchPokemonsPageUseCase

    @Before
    fun setup() {
        fetchPokemonsPageUseCase =
            FetchPokemonsPageUseCase(pokemonNetworkSource, coroutineRule.testDispatcher)
    }

    @Test
    fun `When use case invoked then result success`() = coroutineRule
        .runBlockingTest {
            val params = FetchPokemonsPageUseCase.Params()
            val urls =
                listOf(
                    PokemonUrlJson("url1"),
                    PokemonUrlJson("url2"),
                    PokemonUrlJson("url3")
                )

            val pokemonUrl = PokemonJson(
                1, "name", SpriteJson(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                )
            )
            //WHEN
            whenever(
                pokemonNetworkSource.getPokemonUrlsPage(
                    params.limit,
                    params.offset
                )
            ).thenReturn(urls)
            whenever(pokemonNetworkSource.getPokemonForUrl(any())).thenReturn(pokemonUrl)
            //GIVEN
            val test = fetchPokemonsPageUseCase(params)
            //VERIFY
            assert(test is Result.Success)
            Assert.assertEquals(3, (test as Result.Success).data.size)
        }

    @Test
    fun `When use case invoked then result error after fetching list of urls`() = coroutineRule
        .runBlockingTest {
            val errorMessage = "Cannot fetch urls"
            val error = RuntimeException(errorMessage)
            val params = FetchPokemonsPageUseCase.Params()
            //WHEN
            whenever(
                pokemonNetworkSource.getPokemonUrlsPage(
                    params.limit,
                    params.offset
                )
            ).thenThrow(error)

            //GIVEN
            val test = fetchPokemonsPageUseCase(params)

            //VERIFY
            assert(test is Result.Error)
            Assert.assertEquals(
                errorMessage,
                (test as Result.Error).exception.localizedMessage
            )
        }

    @Test
    fun `When use case invoked then result error after fetching pokemon url`() = coroutineRule
        .runBlockingTest {
            val errorMessage = "Cannot fetch pokemon"
            val error = RuntimeException(errorMessage)
            val urls =
                listOf(
                    PokemonUrlJson("url1"),
                    PokemonUrlJson("url2"),
                    PokemonUrlJson("url3")
                )
            val params = FetchPokemonsPageUseCase.Params()
            //WHEN
            whenever(
                pokemonNetworkSource.getPokemonUrlsPage(
                    params.limit,
                    params.offset
                )
            ).thenReturn(urls)
            whenever(pokemonNetworkSource.getPokemonForUrl(any())).thenThrow(error)

            //GIVEN
            val test = fetchPokemonsPageUseCase(params)

            //VERIFY
            assert(test is Result.Error)
            Assert.assertEquals(
                errorMessage,
                (test as Result.Error).exception.localizedMessage
            )
        }
}