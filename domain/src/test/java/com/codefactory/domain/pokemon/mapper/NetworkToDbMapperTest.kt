package com.codefactory.domain.pokemon.mapper

import com.codefactory.network.pokemon.entity.PokemonJson
import com.codefactory.network.pokemon.entity.SpriteJson
import org.junit.Assert
import org.junit.Test

class NetworkToDbMapperTest {

    private val networkToDbMapper = NetworkToDbMapper()

    @Test
    fun `When db object provided then map it to domain object`() {
        //WHEN
        val sprites = SpriteJson(
            "backFemale", "backShineFemale",
            "backDefault", "frontFemale",
            "frontShinyFemale", "backShiny",
            "frontDefault", "frontShiny"
        )
        val networkObject = PokemonJson(1, "Name", sprites)

        //GIVEN
        val test = networkToDbMapper.transform(networkObject)

        //VERIFY
        Assert.assertEquals(networkObject.id, test.id)
        Assert.assertEquals(networkObject.name, test.name)

        Assert.assertEquals(networkObject.sprites.backFemale, test.sprites.backFemale)
        Assert.assertEquals(networkObject.sprites.backShineFemale, test.sprites.backShineFemale)
        Assert.assertEquals(networkObject.sprites.backDefault, test.sprites.backDefault)
        Assert.assertEquals(networkObject.sprites.frontFemale, test.sprites.frontFemale)
        Assert.assertEquals(networkObject.sprites.frontShinyFemale, test.sprites.frontShinyFemale)
        Assert.assertEquals(networkObject.sprites.backShiny, test.sprites.backShiny)
        Assert.assertEquals(networkObject.sprites.frontDefault, test.sprites.frontDefault)
        Assert.assertEquals(networkObject.sprites.frontShiny, test.sprites.frontShiny)
    }
}