package com.codefactory.domain.pokemon.mapper

import com.codefactory.domain.pokemon.entity.Pokemon
import com.codefactory.shared.domain.Mapper
import com.codefactory.storage.pokemon.entity.PokemonDb
import javax.inject.Inject

class DbToDomainMapper @Inject constructor() : Mapper<PokemonDb, Pokemon> {
    override fun transform(input: PokemonDb): Pokemon = with(input) {
        Pokemon(id, name, sprites.frontDefault)
    }
}