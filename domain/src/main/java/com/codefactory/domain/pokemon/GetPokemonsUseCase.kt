package com.codefactory.domain.pokemon

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.codefactory.domain.pokemon.mapper.DbToDomainMapper
import com.codefactory.domain.pokemon.entity.Pokemon
import com.codefactory.shared.coroutines.IoDispatcher
import com.codefactory.shared.domain.FlowUseCase
import com.codefactory.storage.pokemon.PokemonStorageSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@ExperimentalPagingApi
class GetPokemonsUseCase @Inject constructor(
    private val getPokemonsMediator: GetPokemonsMediator,
    private val pokemonStorageSource: PokemonStorageSource,
    private val dbToDomainMapper: DbToDomainMapper,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : FlowUseCase<Any, PagingData<Pokemon>>(dispatcher) {

    override fun execute(parameters: Any): Flow<PagingData<Pokemon>> {
        return Pager(
            config = PagingConfig(pageSize = 20, enablePlaceholders = false),
            remoteMediator = getPokemonsMediator,
            pagingSourceFactory = { pokemonStorageSource.getAllPaged() }
        )
            .flow
            .map { data -> data.mapSync { dbToDomainMapper.transform(it) } }
    }
}