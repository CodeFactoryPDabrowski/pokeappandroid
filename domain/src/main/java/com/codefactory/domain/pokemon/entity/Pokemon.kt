package com.codefactory.domain.pokemon.entity

data class Pokemon(val id: Int, val name: String, val frontImageUrl: String?)