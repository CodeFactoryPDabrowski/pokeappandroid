package com.codefactory.domain.pokemon

import com.codefactory.network.pokemon.PokemonNetworkSource
import com.codefactory.network.pokemon.entity.PokemonJson
import com.codefactory.shared.coroutines.IoDispatcher
import com.codefactory.shared.domain.ResultUseCase
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject

class FetchPokemonsPageUseCase @Inject constructor(
    private val pokemonNetworkSource: PokemonNetworkSource,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : ResultUseCase<FetchPokemonsPageUseCase.Params, List<PokemonJson>>(dispatcher) {

    override suspend fun execute(
        parameters: Params,
        coroutineScope: CoroutineScope
    ): List<PokemonJson> {
        Timber.d("Current thread: ${Thread.currentThread().name}")
        val urls = pokemonNetworkSource.getPokemonUrlsPage(parameters.limit, parameters.offset)
        val pokemonsDeferred = mutableListOf<Deferred<PokemonJson>>()

        urls.forEach { pokemonUrlJson ->
            pokemonsDeferred.add(coroutineScope.async {
                pokemonNetworkSource.getPokemonForUrl(pokemonUrlJson.url)
            })
        }

        return pokemonsDeferred.awaitAll()
    }

    data class Params(val limit: Int = 25, val offset: Int = 0)
}