package com.codefactory.domain.pokemon.mapper

import com.codefactory.network.pokemon.entity.PokemonJson
import com.codefactory.shared.domain.Mapper
import com.codefactory.storage.pokemon.entity.PokemonDb
import com.codefactory.storage.pokemon.entity.SpritesDb
import javax.inject.Inject

class NetworkToDbMapper @Inject constructor() : Mapper<PokemonJson, PokemonDb> {
    override fun transform(input: PokemonJson): PokemonDb = with(input) {
        PokemonDb(
            id,
            name,
            SpritesDb(
                sprites.frontDefault,
                sprites.backFemale,
                sprites.backShineFemale,
                sprites.backDefault,
                sprites.frontFemale,
                sprites.frontShinyFemale,
                sprites.backShiny,
                sprites.frontShiny
            )
        )
    }
}