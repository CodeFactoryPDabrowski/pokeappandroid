package com.codefactory.domain.pokemon

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.codefactory.domain.pokemon.mapper.NetworkToDbMapper
import com.codefactory.shared.domain.Result
import com.codefactory.shared.domain.successOr
import com.codefactory.storage.pokemon.PokemonStorageSource
import com.codefactory.storage.pokemon.entity.PokemonDb
import com.codefactory.storage.pokemon.remotekey.RemoteKeyStorageSource
import com.codefactory.storage.pokemon.remotekey.entity.RemoteKeyDb
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

@ExperimentalPagingApi
class GetPokemonsMediator @Inject constructor(
    private val fetchPokemonsPageUseCase: FetchPokemonsPageUseCase,
    private val pokemonStorageSource: PokemonStorageSource,
    private val remoteKeyStorageSource: RemoteKeyStorageSource,
    private val networkToDbMapper: NetworkToDbMapper
) : RemoteMediator<Int, PokemonDb>() {

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, PokemonDb>
    ): MediatorResult {
        val offset = calculateOffset(loadType, state)

        try {
            Timber.d("Params to fetch: size: ${state.config.pageSize}, offset: $offset")
            if (offset == INVALID_PAGE) {
                Timber.d("Nothing to fetch for current params")
                return MediatorResult.Success(endOfPaginationReached = true)
            }
            val pokemonsResult =
                fetchPokemonsPageUseCase(
                    FetchPokemonsPageUseCase.Params(
                        state.config.pageSize,
                        offset
                    )
                )

            if (pokemonsResult is Result.Error) {
                Timber.d("Cannot fetch data: ${pokemonsResult.exception}")
                return MediatorResult.Error(pokemonsResult.exception)
            }

            val pokemons = pokemonsResult.successOr(emptyList())
                .map { networkToDbMapper.transform(it) }
            val endOfPaginationReached = pokemons.isEmpty()
            savePokemonsPage(pokemons, offset, state, loadType)

            Timber.d("Operation completed")
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            Timber.d("Cannot fetch data: $exception")
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            Timber.d("Cannot fetch data: $exception")
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun savePokemonsPage(
        pokemons: List<PokemonDb>,
        offset: Int,
        state: PagingState<Int, PokemonDb>,
        loadType: LoadType
    ) {
        val nextKey = offset + state.config.pageSize
        val previousKey = if (offset == POKEMON_FIRST_PAGE) {
            null
        } else {
            offset
        }
        Timber.d("Previous key: $offset, next key: $nextKey")
        pokemonStorageSource.saveAll(
            pokemons = pokemons,
            loadType == LoadType.REFRESH,
            nextKey,
            previousKey
        )
    }

    private suspend fun calculateOffset(
        loadType: LoadType,
        state: PagingState<Int, PokemonDb>
    ): Int =
        when (loadType) {
            LoadType.REFRESH -> {
                Timber.d("Refreshing list")
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextOffset?.minus(state.config.pageSize) ?: POKEMON_FIRST_PAGE
            }
            LoadType.PREPEND -> {
                Timber.d("Prepending")
                val remoteKeys = getRemoteKeyForFirstItem(state)
                if (remoteKeys == null) {
                    POKEMON_FIRST_PAGE
                } else {
                    remoteKeys.previousOffset ?: INVALID_PAGE
                }
            }
            LoadType.APPEND -> {
                Timber.d("Appending")
                val remoteKeys = getRemoteKeyForLastItem(state)
                if (remoteKeys?.nextOffset == null) {
                    POKEMON_FIRST_PAGE
                } else {
                    remoteKeys.nextOffset ?: INVALID_PAGE
                }
            }
        }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, PokemonDb>): RemoteKeyDb? {
        return state.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { pokemon ->
                remoteKeyStorageSource.get(pokemon.id)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, PokemonDb>): RemoteKeyDb? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { pokemon ->
                remoteKeyStorageSource.get(pokemon.id)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, PokemonDb>
    ): RemoteKeyDb? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { pokemonId ->
                remoteKeyStorageSource.get(pokemonId)
            }
        }
    }

    companion object {
        private const val INVALID_PAGE = -1
        private const val POKEMON_FIRST_PAGE = 0
    }
}