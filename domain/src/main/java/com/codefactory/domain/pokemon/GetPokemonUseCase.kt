package com.codefactory.domain.pokemon

import com.codefactory.domain.pokemon.entity.Pokemon
import com.codefactory.domain.pokemon.mapper.DbToDomainMapper
import com.codefactory.shared.coroutines.IoDispatcher
import com.codefactory.shared.domain.Result
import com.codefactory.shared.domain.ResultFlowUseCase
import com.codefactory.storage.pokemon.PokemonStorageSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetPokemonUseCase @Inject constructor(
    private val pokemonStorageSource: PokemonStorageSource,
    private val dbToDomainMapper: DbToDomainMapper,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : ResultFlowUseCase<GetPokemonUseCase.Params, Pokemon>(dispatcher) {

    override fun execute(parameters: Params): Flow<Result<Pokemon>> {
        return pokemonStorageSource.get(
            parameters.id
        ).map { Result.Success(dbToDomainMapper.transform(it)) }
    }

    data class Params(val id: Int)
}