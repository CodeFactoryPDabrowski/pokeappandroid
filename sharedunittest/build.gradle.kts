import com.codefactory.pokeapp.gradle.dependency.Dependencies.BaseTest
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Coroutines
import com.codefactory.pokeapp.gradle.extension.bulkImplementation

plugins {
    id("com.android.library")
    id("module-plugin")
}

dependencies {
    bulkImplementation(BaseTest.Test())
    bulkImplementation(Coroutines.Test())
}