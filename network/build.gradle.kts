import com.codefactory.pokeapp.gradle.dependency.Dependencies.Networking
import com.codefactory.pokeapp.gradle.extension.bulkApi
import com.codefactory.pokeapp.gradle.extension.bulkKapt

plugins {
    id("com.android.library")
    id("module-plugin")
    id("di-plugin")
}

dependencies {
    implementation(project(":shared"))
    bulkApi(Networking.Impl())
    bulkKapt(Networking.MoshiCodeGen())
}