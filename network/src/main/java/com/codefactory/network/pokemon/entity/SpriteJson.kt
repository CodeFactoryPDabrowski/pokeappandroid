package com.codefactory.network.pokemon.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SpriteJson(
    @Json(name = "back_female")
    val backFemale: String?,
    @Json(name = "back_shine_female")
    val backShineFemale: String?,
    @Json(name = "back_default")
    val backDefault: String?,
    @Json(name = "front_female")
    val frontFemale: String?,
    @Json(name = "front_shine_female")
    val frontShinyFemale: String?,
    @Json(name = "back_shiny")
    val backShiny: String?,
    @Json(name = "front_default")
    val frontDefault: String?,
    @Json(name = "front_shiny")
    val frontShiny: String?
)