package com.codefactory.network.pokemon.retrofit

import com.codefactory.network.pokemon.PokemonNetworkSource
import com.codefactory.network.pokemon.entity.PokemonJson
import com.codefactory.network.pokemon.api.PokemonApi
import com.codefactory.network.pokemon.entity.PokemonUrlJson
import javax.inject.Inject

class RetrofitPokemonSource @Inject constructor(
    private val pokemonApi: PokemonApi
) : PokemonNetworkSource {

    override suspend fun getPokemonUrlsPage(limit: Int, offset: Int): List<PokemonUrlJson> =
        pokemonApi.getPokemonUrls(limit, offset).results

    override suspend fun getPokemonForUrl(url: String): PokemonJson = pokemonApi.getPokemon(url)
}