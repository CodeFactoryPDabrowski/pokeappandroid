package com.codefactory.network.pokemon.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PokemonUrlsJson(@Json(name = "results") val results: List<PokemonUrlJson>)

@JsonClass(generateAdapter = true)
data class PokemonUrlJson(@Json(name = "url") val url: String)