package com.codefactory.network.pokemon

import com.codefactory.network.pokemon.entity.PokemonJson
import com.codefactory.network.pokemon.entity.PokemonUrlJson

interface PokemonNetworkSource {
    suspend fun getPokemonUrlsPage(limit: Int = 25, offset: Int = 0): List<PokemonUrlJson>

    suspend fun getPokemonForUrl(url: String): PokemonJson
}