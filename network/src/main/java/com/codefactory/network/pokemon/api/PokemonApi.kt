package com.codefactory.network.pokemon.api

import com.codefactory.network.pokemon.entity.PokemonJson
import com.codefactory.network.pokemon.entity.PokemonUrlsJson
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface PokemonApi {

    @GET("pokemon")
    suspend fun getPokemonUrls(
        @Query("limit") limit: Int = 25,
        @Query("offset") offset: Int = 0
    ): PokemonUrlsJson

    @GET
    suspend fun getPokemon(@Url pokemonUrl: String): PokemonJson
}