package com.codefactory.network

import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import com.codefactory.network.pokemon.PokemonNetworkSource
import com.codefactory.network.pokemon.retrofit.RetrofitPokemonSource
import com.codefactory.network.pokemon.api.PokemonApi
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module(includes = [NonAuthNetworkModule.Declarations::class])
object NonAuthNetworkModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder().build()

    @Singleton
    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder().build()

    @Singleton
    @Provides
    fun provideRetrofit(client: OkHttpClient, moshi: Moshi): Retrofit = Retrofit.Builder()
        .baseUrl("https://pokeapi.co/api/v2/") //TODO: Should be part of gradle configuration
        .client(client)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

    @Provides
    fun providePokemonApi(retrofit: Retrofit): PokemonApi = retrofit.create(PokemonApi::class.java)

    @InstallIn(ApplicationComponent::class)
    @Module
    internal interface Declarations {
        @Binds
        @Singleton
        fun bindsPokemonNetworkSource(retrofitSource: RetrofitPokemonSource): PokemonNetworkSource
    }

}