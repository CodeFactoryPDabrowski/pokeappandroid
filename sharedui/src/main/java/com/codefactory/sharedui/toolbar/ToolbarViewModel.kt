package com.codefactory.sharedui.toolbar

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class ToolbarViewModel @ViewModelInject constructor() : ViewModel() {

    private val _configuration = MutableLiveData<ToolbarConfiguration>()
    val configuration: LiveData<ToolbarConfiguration> = _configuration

    fun updateTitle(title: String) {
        _configuration.value = ToolbarConfiguration(title)
    }

    data class ToolbarConfiguration(val title: String)
}