package com.codefactory.sharedui.adapter

fun interface ItemClickListener<in T> {
    fun onClick(item: T)
}