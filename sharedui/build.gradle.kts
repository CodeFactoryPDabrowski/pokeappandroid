import com.codefactory.pokeapp.gradle.dependency.Dependencies.AndroidSupport
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Coil
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Lifecycle
import com.codefactory.pokeapp.gradle.extension.bulkApi
import com.codefactory.pokeapp.gradle.extension.bulkKapt

plugins {
    id("com.android.library")
    id("module-plugin")
    id("di-plugin")
}

dependencies {
    bulkApi(AndroidSupport.Impl())
    bulkApi(Lifecycle.Impl())
    bulkKapt(Lifecycle.Kapt())
    bulkApi(Coil.Impl())
}