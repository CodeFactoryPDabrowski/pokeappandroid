import com.codefactory.pokeapp.gradle.dependency.Dependencies.BaseTest
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Coroutines
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Storage.Room
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Storage.RoomProcessor
import com.codefactory.pokeapp.gradle.extension.bulkAndroidTestImplementation
import com.codefactory.pokeapp.gradle.extension.bulkApi
import com.codefactory.pokeapp.gradle.extension.bulkKapt

plugins {
    id("com.android.library")
    id("module-plugin")
    id("di-plugin")
}

dependencies {
    implementation(project(":shared"))
    bulkApi(Room())
    bulkKapt(RoomProcessor())
    bulkAndroidTestImplementation(BaseTest.Test())
    bulkAndroidTestImplementation(BaseTest.AndroidTest())
    androidTestImplementation(project(":sharedunittest"))
    bulkAndroidTestImplementation(Coroutines.Test())
}