package com.codefactory.storage.pokemon.room

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.codefactory.pokeapp.sharedunittest.CoroutineScope
import com.codefactory.pokeapp.sharedunittest.CoroutineTestRule
import com.codefactory.pokeapp.sharedunittest.runBlockingTest
import com.codefactory.storage.AppDatabase
import com.codefactory.storage.pokemon.entity.PokemonDb
import com.codefactory.storage.pokemon.entity.SpritesDb
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asExecutor
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class RoomPokemonSourceTest {

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var roomPokemonSource: RoomPokemonSource
    private lateinit var appDatabase: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).allowMainThreadQueries()
            .setTransactionExecutor(coroutineRule.testDispatcher.asExecutor())
            .setQueryExecutor(coroutineRule.testDispatcher.asExecutor())
            .build()
        roomPokemonSource = RoomPokemonSource(appDatabase)
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        appDatabase.close()
    }

    @Test
    fun whenSaveAllThenAllSaved() =
        coroutineRule.runBlockingTest {
            val spritesDb = SpritesDb(
                "test",
                "test",
                "test",
                "test",
                "test",
                "test",
                "test",
                "test"
            )
            val pokemon1 = PokemonDb(1, "name1", spritesDb)
            val pokemon2 = PokemonDb(2, "name2", spritesDb)

            val pokemons = listOf(pokemon1, pokemon2)
            //GIVEN
            coroutineRule.CoroutineScope().launch(Dispatchers.IO) {
                roomPokemonSource.saveAll(pokemons, true, null, null)
            }

            //VERIFY
            val testPokemons =
                appDatabase.pokemonDao().getAll() //TODO race condition, sometimes it's working
            val testKeys = appDatabase.remoteKeyDao().getAll()
            assertEquals(2, testPokemons.size)
            assertEquals(2, testKeys.size)
        }

    @Test
    fun whenSaveAllThenDuplicatesNotSaved() =
        coroutineRule.runBlockingTest {
            val spritesDb = SpritesDb(
                "test",
                "test",
                "test",
                "test",
                "test",
                "test",
                "test",
                "test"
            )
            val pokemon1 = PokemonDb(1, "name1", spritesDb)

            val pokemons = listOf(pokemon1, pokemon1)
            //GIVEN
            coroutineRule.CoroutineScope().launch(Dispatchers.IO) {
                roomPokemonSource.saveAll(pokemons, true, null, null)
            }

            //VERIFY
            val testPokemons = appDatabase.pokemonDao().getAll()
            val testKeys = appDatabase.remoteKeyDao().getAll()
            assertEquals(1, testPokemons.size)
            assertEquals(1, testKeys.size)
        }

    @Test
    fun whenGetThenExpectedPokemon() = coroutineRule.testDispatcher.runBlockingTest {
        val spritesDb = SpritesDb(
            "test",
            "test",
            "test",
            "test",
            "test",
            "test",
            "test",
            "test"
        )

        //GIVEN
        val pokemonAsync = async {
            roomPokemonSource.get(1)
                .filterNotNull()// because first item is null(db is empty)
                .first()
        }

        val pokemon1 = PokemonDb(1, "name1", spritesDb)
        appDatabase.pokemonDao().saveAll(listOf(pokemon1))

        assert(pokemonAsync.await() == pokemon1)
    }
}
