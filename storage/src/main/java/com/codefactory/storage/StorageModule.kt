package com.codefactory.storage

import android.content.Context
import com.codefactory.storage.pokemon.PokemonStorageSource
import com.codefactory.storage.pokemon.remotekey.RemoteKeyStorageSource
import com.codefactory.storage.pokemon.remotekey.room.RemoteKeyDao
import com.codefactory.storage.pokemon.remotekey.room.RoomRemoteKeySource
import com.codefactory.storage.pokemon.room.PokemonDao
import com.codefactory.storage.pokemon.room.RoomPokemonSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module(includes = [StorageModule.Declarations::class])
object StorageModule {

    @Singleton
    @Provides
    fun provideDb(@ApplicationContext context: Context): AppDatabase = AppDatabase.build(context)

    @Singleton
    @Provides
    fun providePokemonDao(appDatabase: AppDatabase): PokemonDao = appDatabase.pokemonDao()

    @Singleton
    @Provides
    fun provideRemoteKeyDao(appDatabase: AppDatabase): RemoteKeyDao = appDatabase.remoteKeyDao()

    @InstallIn(ApplicationComponent::class)
    @Module
    internal interface Declarations {
        @Binds
        @Singleton
        fun bindsPokemonStorageSource(roomSource: RoomPokemonSource): PokemonStorageSource

        @Binds
        @Singleton
        fun bindsRemoteKeyStorageSource(roomSource: RoomRemoteKeySource): RemoteKeyStorageSource
    }

}