package com.codefactory.storage.pokemon.remotekey.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.codefactory.storage.pokemon.remotekey.entity.RemoteKeyDb

@Dao
abstract class RemoteKeyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun saveAll(remoteKey: List<RemoteKeyDb>)

    @Query("SELECT * FROM remote_keys WHERE pokemonId = :pokemonId")
    abstract suspend fun get(pokemonId: Int): RemoteKeyDb?

    @Query("DELETE FROM remote_keys")
    abstract suspend fun removeAll()

    @Query("SELECT * FROM remote_keys ORDER BY pokemonId ASC")
    abstract fun getAll(): List<RemoteKeyDb>
}