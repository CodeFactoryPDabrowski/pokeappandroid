package com.codefactory.storage.pokemon.remotekey

import com.codefactory.storage.pokemon.remotekey.entity.RemoteKeyDb

interface RemoteKeyStorageSource {
    suspend fun get(pokemonId: Int): RemoteKeyDb?
}