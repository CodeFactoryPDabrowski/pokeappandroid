package com.codefactory.storage.pokemon.room

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.codefactory.storage.pokemon.entity.PokemonDb
import kotlinx.coroutines.flow.Flow

@Dao
abstract class PokemonDao {

    @Insert(onConflict = REPLACE)
    abstract suspend fun saveAll(pokemons: List<PokemonDb>)

    @Query("DELETE FROM pokemons")
    abstract suspend fun removeAll()

    @Query("SELECT * FROM pokemons ORDER BY id ASC")
    abstract fun getAllPaged(): PagingSource<Int, PokemonDb>

    @Query("SELECT * FROM pokemons WHERE id = :id")
    abstract fun get(id: Int): Flow<PokemonDb>

    @Query("SELECT * FROM pokemons ORDER BY id ASC")
    abstract fun getAll(): List<PokemonDb>
}