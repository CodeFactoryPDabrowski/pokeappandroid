package com.codefactory.storage.pokemon

import androidx.paging.PagingSource
import com.codefactory.storage.pokemon.entity.PokemonDb
import kotlinx.coroutines.flow.Flow

interface PokemonStorageSource {

    suspend fun saveAll(
        pokemons: List<PokemonDb>,
        clearOld: Boolean,
        nextOffset: Int?,
        previousOffset: Int?
    )

    fun getAllPaged(): PagingSource<Int, PokemonDb>

    fun get(id: Int): Flow<PokemonDb>
}