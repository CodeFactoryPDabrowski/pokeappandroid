package com.codefactory.storage.pokemon.remotekey.room

import com.codefactory.storage.pokemon.remotekey.RemoteKeyStorageSource
import com.codefactory.storage.pokemon.remotekey.entity.RemoteKeyDb
import javax.inject.Inject

class RoomRemoteKeySource @Inject constructor(
    private val remoteKeyDao: RemoteKeyDao
) : RemoteKeyStorageSource {
    override suspend fun get(pokemonId: Int): RemoteKeyDb? = remoteKeyDao.get(pokemonId)
}