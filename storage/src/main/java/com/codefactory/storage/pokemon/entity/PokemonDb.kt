package com.codefactory.storage.pokemon.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemons")
data class PokemonDb(
    @PrimaryKey val id: Int,
    val name: String,
    @Embedded(prefix = "sprite_")
    val sprites: SpritesDb
)