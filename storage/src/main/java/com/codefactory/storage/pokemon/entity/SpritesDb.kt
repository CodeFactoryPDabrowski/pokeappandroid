package com.codefactory.storage.pokemon.entity

data class SpritesDb(
    val frontDefault: String?,
    val backFemale: String?,
    val backShineFemale: String?,
    val backDefault: String?,
    val frontFemale: String?,
    val frontShinyFemale: String?,
    val backShiny: String?,
    val frontShiny: String?
)