package com.codefactory.storage.pokemon.remotekey.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "remote_keys")
data class RemoteKeyDb(
    @PrimaryKey val pokemonId: Int,
    val nextOffset: Int?,
    val previousOffset: Int?
)