package com.codefactory.storage.pokemon.room

import androidx.paging.PagingSource
import androidx.room.withTransaction
import com.codefactory.storage.AppDatabase
import com.codefactory.storage.pokemon.PokemonStorageSource
import com.codefactory.storage.pokemon.entity.PokemonDb
import com.codefactory.storage.pokemon.remotekey.entity.RemoteKeyDb
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RoomPokemonSource @Inject constructor(
    private val appDatabase: AppDatabase
) : PokemonStorageSource {

    private val pokemonDao = appDatabase.pokemonDao()

    override suspend fun saveAll(
        pokemons: List<PokemonDb>,
        clearOld: Boolean,
        nextOffset: Int?,
        previousOffset: Int?
    ) {
        appDatabase.withTransaction {
            if (clearOld) {
                pokemonDao.removeAll()
                appDatabase.remoteKeyDao().removeAll()
            }
            val keys = pokemons.map {
                RemoteKeyDb(
                    pokemonId = it.id,
                    previousOffset = previousOffset,
                    nextOffset = nextOffset
                )
            }
            appDatabase.remoteKeyDao().saveAll(keys)
            pokemonDao.saveAll(pokemons)
        }
    }

    override fun getAllPaged(): PagingSource<Int, PokemonDb> =
        pokemonDao.getAllPaged()

    override fun get(id: Int): Flow<PokemonDb> = pokemonDao.get(id)
}