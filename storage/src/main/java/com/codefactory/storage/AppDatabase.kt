package com.codefactory.storage

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.codefactory.storage.pokemon.entity.PokemonDb
import com.codefactory.storage.pokemon.remotekey.entity.RemoteKeyDb
import com.codefactory.storage.pokemon.remotekey.room.RemoteKeyDao
import com.codefactory.storage.pokemon.room.PokemonDao

@Database(entities = [PokemonDb::class, RemoteKeyDb::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun pokemonDao(): PokemonDao

    abstract fun remoteKeyDao(): RemoteKeyDao

    companion object {
        private const val DATABASE_NAME = "PokeDb"

        fun build(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                .build()
        }
    }
}