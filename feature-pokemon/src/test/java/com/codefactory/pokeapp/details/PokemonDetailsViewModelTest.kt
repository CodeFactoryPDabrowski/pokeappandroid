package com.codefactory.pokeapp.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.codefactory.domain.pokemon.GetPokemonUseCase
import com.codefactory.domain.pokemon.entity.Pokemon
import com.codefactory.pokeapp.sharedunittest.CoroutineTestRule
import com.codefactory.pokeapp.sharedunittest.runBlockingTest
import com.codefactory.shared.domain.Result
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.flowOf
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PokemonDetailsViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @Mock
    private lateinit var getPokemonUseCase: GetPokemonUseCase

    @Mock
    private lateinit var observer: Observer<Result<Pokemon>>

    @InjectMocks
    private lateinit var viewModel: PokemonDetailsViewModel

    @Test
    fun `When new details triggered then load details success`() = coroutineRule.runBlockingTest {
        val params = GetPokemonUseCase.Params(1)
        val success = Result.Success(Pokemon(1, "name", "url"))
        //WHEN
        whenever(getPokemonUseCase(params)).thenReturn(flowOf(success))

        //GIVEN
        viewModel.detailsTrigger.value = params

        viewModel.details.observeForever(observer)

        //VERIFY
        verify(observer).onChanged(success)

        viewModel.details.removeObserver(observer)
    }

    @Test
    fun `When new details triggered then load details error`() = coroutineRule.runBlockingTest {
        val params = GetPokemonUseCase.Params(1)
        val error = Result.Error(Exception("cannot load details"))
        //WHEN
        whenever(getPokemonUseCase(params)).thenReturn(flowOf(error))

        //GIVEN
        viewModel.detailsTrigger.value = params

        viewModel.details.observeForever(observer)

        //VERIFY
        verify(observer).onChanged(error)

        viewModel.details.removeObserver(observer)
    }

}