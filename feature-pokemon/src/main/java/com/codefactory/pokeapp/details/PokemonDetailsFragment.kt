package com.codefactory.pokeapp.details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import coil.load
import com.codefactory.domain.pokemon.GetPokemonUseCase
import com.codefactory.domain.pokemon.entity.Pokemon
import com.codefactory.pokeapp.details.databinding.FragmentPokemonDetailsBinding
import com.codefactory.shared.domain.Result
import com.codefactory.sharedui.binding.viewBinding
import com.codefactory.sharedui.toolbar.ToolbarViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PokemonDetailsFragment : Fragment(R.layout.fragment_pokemon_details) {

    private val binding by viewBinding(FragmentPokemonDetailsBinding::bind)
    private val viewModel: PokemonDetailsViewModel by viewModels()
    private val toolbarViewModel: ToolbarViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = arguments?.getInt(ARG_ID)
        require(id != null) { "Pokemon id cannot be null!" }
        viewModel.detailsTrigger.value = GetPokemonUseCase.Params(id)
        viewModel.details.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Success ->
                    loadDetails(result.data)
            }
        }
    }

    private fun loadDetails(pokemon: Pokemon) {
        toolbarViewModel.updateTitle(pokemon.name)
        binding.pokemonAvatar.load(pokemon.frontImageUrl) {
            placeholder(R.drawable.ic_pokemon_placeholder)
        }
    }

    companion object {
        private const val ARG_ID = "pokemon_id"
    }
}