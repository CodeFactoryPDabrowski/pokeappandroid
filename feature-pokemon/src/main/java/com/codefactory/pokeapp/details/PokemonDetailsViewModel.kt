package com.codefactory.pokeapp.details

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.codefactory.domain.pokemon.GetPokemonUseCase
import com.codefactory.domain.pokemon.entity.Pokemon
import com.codefactory.shared.domain.Result

class PokemonDetailsViewModel @ViewModelInject constructor(
    private val getPokemonUseCase: GetPokemonUseCase
) : ViewModel() {

    val detailsTrigger = MutableLiveData<GetPokemonUseCase.Params>()

    val details: LiveData<Result<Pokemon>> =
        detailsTrigger.switchMap { params ->
            getPokemonUseCase(params).asLiveData(viewModelScope.coroutineContext)
        }
}