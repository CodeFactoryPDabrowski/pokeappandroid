import com.codefactory.pokeapp.gradle.dependency.Dependencies.Insetter
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Navigation
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Startup
import com.codefactory.pokeapp.gradle.extension.bulkImplementation

plugins {
    id("com.android.application")
    id("module-plugin")
    id("di-plugin")
}

dependencies {
    implementation(project(":domain"))
    implementation(project(":sharedui"))
    implementation(project(":network"))
    implementation(project(":storage"))
    implementation(project(":shared"))
    implementation(project(":feature-pokemons"))
    implementation(project(":feature-pokemon"))
    bulkImplementation(Navigation.Impl())
    bulkImplementation(Startup())
    bulkImplementation(Insetter())
}