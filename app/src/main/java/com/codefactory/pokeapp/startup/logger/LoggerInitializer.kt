package com.codefactory.pokeapp.startup.logger

import android.content.Context
import com.codefactory.pokeapp.BuildConfig
import com.codefactory.pokeapp.startup.NoDependencyInitializer
import timber.log.Timber

class LoggerInitializer : NoDependencyInitializer() {

    override fun create(context: Context) {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}