package com.codefactory.pokeapp.startup

import androidx.startup.Initializer

abstract class NoDependencyInitializer : Initializer<Unit> {
    
    override fun dependencies() = emptyList<Class<out Initializer<*>>>()
}