package com.codefactory.pokeapp.startup.image

import android.content.Context
import coil.Coil
import coil.ImageLoader
import com.codefactory.pokeapp.startup.NoDependencyInitializer

class ImageLoaderInitializer : NoDependencyInitializer() {

    override fun create(context: Context) {
        Coil.setImageLoader {
            ImageLoader.Builder(context)
                // Hardware bitmaps break with our transitions, disable them for now
                .allowHardware(false)
                .allowRgb565(true)
                // Since we don't use hardware bitmaps, we can pool bitmaps and use a higher
                // ratio of memory
                .bitmapPoolPercentage(0.5)
                .build()
        }
    }
}