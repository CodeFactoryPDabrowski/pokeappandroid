package com.codefactory.pokeapp.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.codefactory.pokeapp.R
import com.codefactory.pokeapp.databinding.ActivityMainBinding
import com.codefactory.sharedui.binding.viewBinding
import com.codefactory.sharedui.toolbar.ToolbarViewModel
import dagger.hilt.android.AndroidEntryPoint
import dev.chrisbanes.insetter.Insetter
import dev.chrisbanes.insetter.Side
import java.util.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)
    private val toolbarViewModel: ToolbarViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setWindowFlags()
        initToolbar()
    }

    private fun initToolbar() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.mainNavHost) as NavHostFragment
        val navController = navHostFragment.navController
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)
        toolbarViewModel.configuration.observe(this) { configuration ->
            binding.toolbar.title = configuration.title.capitalize(Locale.getDefault())
        }
    }

    private fun setWindowFlags() {
        Insetter.builder()
            .applySystemWindowInsetsToPadding(Side.LEFT or Side.RIGHT or Side.TOP)
            .consumeSystemWindowInsets(Insetter.CONSUME_AUTO)
            .applyToView(binding.root)

        Insetter.setEdgeToEdgeSystemUiFlags(binding.root, true)
    }
}