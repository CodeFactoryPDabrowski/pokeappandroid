package com.codefactory.pokeapp.gradle.plugin

import com.android.build.gradle.BaseExtension
import com.codefactory.pokeapp.gradle.dependency.Dependencies
import com.codefactory.pokeapp.gradle.extension.bulkImplementation
import com.codefactory.pokeapp.gradle.extension.bulkKapt
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

class DiPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.plugins.apply("dagger.hilt.android.plugin")
        project.plugins.apply("kotlin-android")
        project.plugins.apply("kotlin-kapt")
        val androidExtension = project.extensions.getByName("android")
        if (androidExtension is BaseExtension) {
            applyEssentialDependencies(project)
        }
    }

    private fun applyEssentialDependencies(project: Project) {
        project.dependencies {
            bulkImplementation(Dependencies.Di.Hilt())
            bulkKapt(Dependencies.Di.HiltCompiler())
        }
    }
}