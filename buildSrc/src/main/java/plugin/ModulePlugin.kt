package com.codefactory.pokeapp.gradle.plugin

import com.android.build.gradle.AppExtension
import com.android.build.gradle.BaseExtension
import com.android.build.gradle.LibraryExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.codefactory.pokeapp.gradle.dependency.Dependencies
import com.codefactory.pokeapp.gradle.extension.bulkImplementation
import com.codefactory.pokeapp.gradle.extension.bulkTestImplementation

class ModulePlugin : Plugin<Project> {

    override fun apply(project: Project) {
        project.plugins.apply("kotlin-android")
        project.plugins.apply("kotlin-kapt")
        val androidExtension = project.extensions.getByName("android")
        if (androidExtension is BaseExtension) {
            androidExtension.apply {
                setupBaseAppSetup()
                setupJvm18(project)
                setupProguardFiles()
                setupKotlinDir()
                setupViewBinding()
                setupLintOptions()
                excludeMetaFiles()
                applyEssentialDependencies(project)
            }
        }
    }

    private fun BaseExtension.setupBaseAppSetup() {
        compileSdkVersion(COMPILE_SDK_VERSION)
        defaultConfig {
            if (this is AppExtension) {
                applicationId = "com.codefactory.pokeapp"
            }
            minSdkVersion(MIN_SDK_VERSION)
            targetSdkVersion(TARGET_SDK_VERSION)
            versionCode = 1
            versionName = VERSION_NAME
            testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        }
    }

    private fun BaseExtension.setupJvm18(project: Project) {
        compileOptions {
            coreLibraryDesugaringEnabled = true
            sourceCompatibility = JavaVersion.VERSION_1_8
            targetCompatibility = JavaVersion.VERSION_1_8
        }
        with(project) {
            tasks.withType(KotlinCompile::class.java).configureEach {
                kotlinOptions {
                    jvmTarget = JavaVersion.VERSION_1_8.toString()
                    freeCompilerArgs =
                        listOf(USE_EXPERIMENTAL_COROUTINES_FLAG, USE_EXPERIMENTAL_TIME)
                }
            }
            dependencies {
                add(
                    Dependencies.Jdk.DESUGARING_CONFIG_NAME,
                    Dependencies.Jdk.DesugarLibs.desugarJdkLibs
                )
            }
        }
    }

    @Suppress("StringLiteralDuplication")
    private fun BaseExtension.setupProguardFiles() {
        val proguardFile = "proguard-rules.pro"
        when (this) {
            is LibraryExtension -> {
                defaultConfig {
                    consumerProguardFiles(proguardFile)
                }
            }
            is AppExtension -> {
                buildTypes {
                    getByName(DEBUG_BUILD_TYPE_NAME) {
                        versionNameSuffix = "-DEBUG"
                        applicationIdSuffix = ".debug"
                        isDebuggable = true
                        isMinifyEnabled = false
                        isShrinkResources = false
                    }
                    getByName(RELEASE_BUILD_TYPE_NAME) {
                        isDebuggable = false
                        isMinifyEnabled = true
                        isShrinkResources = true
                        isZipAlignEnabled = true
                        proguardFiles(
                            getDefaultProguardFile("proguard-android-optimize.txt"),
                            proguardFile
                        )
                    }
                }
            }
        }
    }

    private fun BaseExtension.setupKotlinDir() {
        sourceSets.onEach { it.java.srcDir("src/${it.name}/kotlin") }
    }

    private fun BaseExtension.setupViewBinding() {
        buildFeatures.viewBinding = true
    }

    private fun BaseExtension.setupLintOptions() {
        lintOptions {
            isAbortOnError = false
            xmlReport = true
            isCheckDependencies = true
            disable("ContentDescription")
        }
    }

    private fun BaseExtension.excludeMetaFiles() {
        packagingOptions {
            exclude("META-INF/**")
            exclude("**/attach_hotspot_windows.dll")
        }
    }

    private fun applyEssentialDependencies(project: Project) {
        project.dependencies {
            bulkImplementation(Dependencies.KotlinCore.Impl())
            bulkTestImplementation(Dependencies.BaseTest.Test())
        }
    }

    companion object {
        private const val COMPILE_SDK_VERSION = 29
        private const val MIN_SDK_VERSION = 23
        private const val TARGET_SDK_VERSION = 29
        private const val VERSION_NAME = "1.0.0"

        private const val USE_EXPERIMENTAL_COROUTINES_FLAG =
            "-Xuse-experimental=kotlinx.coroutines.ExperimentalCoroutinesApi"
        private const val USE_EXPERIMENTAL_TIME =
            "-Xopt-in=kotlin.time.ExperimentalTime"

        const val DEBUG_BUILD_TYPE_NAME = "debug"
        const val RELEASE_BUILD_TYPE_NAME = "release"
    }
}