package com.codefactory.pokeapp.gradle.dependency

import com.codefactory.pokeapp.gradle.dependency.Versions.pagingVersion
import com.codefactory.pokeapp.gradle.dependency.Versions.roomVersion

object Versions {
    const val kotlin = "1.4.0"
    const val appCompatVersion = "1.1.0"
    const val fragmentVersion = "1.2.5"
    const val coilVersion = "0.13.0"
    const val coroutinesVersion = "1.3.9"
    const val lifecycleVersion = "2.2.0"
    const val moshiVersion = "1.9.3"
    const val okHttpVersion = "4.7.2"
    const val retrofitVersion = "2.9.0"
    const val espressoVersion = "3.2.0"
    const val hilt = "2.28-alpha"
    const val hiltJetpack = "1.0.0-alpha01"
    const val compose = "0.1.0-dev14"
    const val roomVersion = "2.3.0-alpha02"
    const val pagingVersion = "3.0.0-alpha06"
    const val nav = "2.3.0"
}

object Dependencies {

    object Navigation {
        object Impl : BaseDependency() {
            private const val navFragment =
                "androidx.navigation:navigation-fragment-ktx:${Versions.nav}"
            private const val navUi = "androidx.navigation:navigation-ui-ktx:${Versions.nav}"
            override val libraries = listOf(navFragment, navUi)
        }
    }

    object KotlinCore {
        object Impl : BaseDependency() {
            private const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"
            private const val kotlinKtx = "androidx.core:core-ktx:1.3.0"
            override val libraries = listOf(kotlinStdLib, kotlinKtx)
        }
    }

    object AndroidSupport {
        object Impl : BaseDependency() {
            private const val appCompat =
                "androidx.appcompat:appcompat:${Versions.appCompatVersion}"
            private const val recyclerView =
                "androidx.recyclerview:recyclerview:${Versions.appCompatVersion}"
            private const val material = "com.google.android.material:material:1.1.0"
            private const val annotation =
                "androidx.annotation:annotation:${Versions.appCompatVersion}"
            private const val constraintLayout = "androidx.constraintlayout:constraintlayout:1.1.3"
            override val libraries = listOf(
                appCompat, recyclerView, material, annotation, constraintLayout
            )
        }
    }

    object AndroidFragment {
        object Impl : BaseDependency() {
            private const val fragment =
                "androidx.fragment:fragment-ktx:${Versions.fragmentVersion}"
            override val libraries = listOf(fragment)
        }
    }

    object Coil {
        object Impl : BaseDependency() {
            private const val coil = "io.coil-kt:coil:${Versions.coilVersion}"
            override val libraries = listOf(coil)
        }
    }

    object Utils {
        object Impl : BaseDependency() {
            private const val timber = "com.jakewharton.timber:timber:4.7.1"
            override val libraries = listOf(timber)
        }
    }

    object Coroutines {
        object Impl : BaseDependency() {
            private const val coroutinesCore =
                "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutinesVersion}"
            private const val coroutinesAndroid =
                "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutinesVersion}"
            override val libraries = listOf(coroutinesCore, coroutinesAndroid)
        }

        object Test : BaseDependency() {
            private const val coroutinesTest =
                "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutinesVersion}"

            const val turbine = "app.cash.turbine:turbine:0.2.1"
            override val libraries = listOf(coroutinesTest, turbine)
        }
    }

    object Lifecycle {
        object Impl : BaseDependency() {
            private const val lifeCycleViewModel =
                "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycleVersion}"
            private const val lifeCycleLiveData =
                "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycleVersion}"
            private const val lifecycleRuntime =
                "androidx.lifecycle:lifecycle-runtime:${Versions.lifecycleVersion}"
            private const val lifecycleJava8 =
                "androidx.lifecycle:lifecycle-common-java8:${Versions.lifecycleVersion}"
            override val libraries =
                listOf(lifeCycleViewModel, lifeCycleLiveData, lifecycleRuntime, lifecycleJava8)
        }

        object Kapt : BaseDependency() {
            private const val lifecycleCompiler =
                "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycleVersion}"
            override val libraries = listOf(lifecycleCompiler)
        }
    }

    object BaseTest {
        object Test : BaseDependency() {
            private const val junit = "junit:junit:4.13"
            private const val truth = "androidx.test.ext:truth:1.3.0"
            private const val archCoreTesting = "androidx.arch.core:core-testing:2.1.0"
            private const val mockitoCore = "org.mockito:mockito-core:3.5.11"
            private const val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0"
            override val libraries = listOf(
                junit, mockitoCore, mockitoKotlin, truth, archCoreTesting
            )
        }

        object AndroidTest : BaseDependency() {
            private const val testCore = "androidx.test:core-ktx:1.2.0"
            private const val mockitoCore = "org.mockito:mockito-core:3.3.3"
            private const val mockitoAndroid = "org.mockito:mockito-android:3.3.3"
            private const val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0"
            private const val espressoCore =
                "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
            private const val espressoIntents =
                "androidx.test.espresso:espresso-intents:${Versions.espressoVersion}"
            private const val espressoContrib =
                "androidx.test.espresso:espresso-contrib:${Versions.espressoVersion}"
            private const val fragmentTesting =
                "androidx.fragment:fragment-testing:${Versions.fragmentVersion}"
            private const val androidxTestExtJunit = "androidx.test.ext:junit:1.1.1"
            private const val androidxTestExtJunitKtx = "androidx.test.ext:junit-ktx:1.1.1"
            private const val kakao = "com.agoda.kakao:kakao:2.3.0"
            override val libraries = listOf(
                espressoCore, espressoIntents, espressoContrib, fragmentTesting, kakao,
                androidxTestExtJunit, androidxTestExtJunitKtx, testCore, mockitoAndroid,
                mockitoKotlin, mockitoCore
            )
        }
    }

    object Networking {
        object Impl : BaseDependency() {
            private const val okHttp = "com.squareup.okhttp3:okhttp:${Versions.okHttpVersion}"
            private const val loggingInterceptor =
                "com.squareup.okhttp3:logging-interceptor:${Versions.okHttpVersion}"
            private const val retrofit =
                "com.squareup.retrofit2:retrofit:${Versions.retrofitVersion}"
            private const val retrofitMoshiAdapter =
                "com.squareup.retrofit2:converter-moshi:${Versions.retrofitVersion}"
            private const val moshi = "com.squareup.moshi:moshi:${Versions.moshiVersion}"
            override val libraries = listOf(
                okHttp, loggingInterceptor, retrofit, retrofitMoshiAdapter, moshi
            )
        }

        object MoshiCodeGen : BaseDependency() {
            private const val codeGen =
                "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshiVersion}"
            override val libraries: List<String>
                get() = listOf(codeGen)
        }

        object Test : BaseDependency() {
            private const val mockWebServer =
                "com.squareup.okhttp3:mockwebserver:${Versions.okHttpVersion}"
            override val libraries = listOf(mockWebServer)
        }
    }

    object Jdk {
        object DesugarLibs : BaseDependency() {
            const val desugarJdkLibs = "com.android.tools:desugar_jdk_libs:1.0.9"
            override val libraries = listOf(desugarJdkLibs)
        }

        const val DESUGARING_CONFIG_NAME = "coreLibraryDesugaring"
    }

    object Di {
        object Hilt : BaseDependency() {
            private const val hiltLib = "com.google.dagger:hilt-android:${Versions.hilt}"
            private const val hiltViewModelLib =
                "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltJetpack}"
            override val libraries: List<String>
                get() = listOf(hiltLib, hiltViewModelLib)

        }

        object HiltCompiler : BaseDependency() {
            private const val hiltCompiler =
                "com.google.dagger:hilt-android-compiler:${Versions.hilt}"
            private const val hiltViewModelCompiler =
                "androidx.hilt:hilt-compiler:${Versions.hiltJetpack}"
            override val libraries: List<String>
                get() = listOf(hiltCompiler, hiltViewModelCompiler)

        }
    }

    object Storage {
        object Room : BaseDependency() {
            private const val roomLib = "androidx.room:room-runtime:$roomVersion"
            private const val roomKtx = "androidx.room:room-ktx:$roomVersion"
            override val libraries: List<String>
                get() = listOf(roomLib, roomKtx)
        }

        object RoomProcessor : BaseDependency() {
            private const val roomProc = "androidx.room:room-compiler:$roomVersion"
            override val libraries: List<String>
                get() = listOf(roomProc)
        }
    }

    object Paging : BaseDependency() {
        private const val paging = "androidx.paging:paging-runtime:$pagingVersion"
        override val libraries: List<String>
            get() = listOf(paging)

    }

    object Palette : BaseDependency() {
        private const val palette = "androidx.palette:palette:1.0.0"

        override val libraries: List<String>
            get() = listOf(palette)
    }

    object Startup : BaseDependency() {
        private const val startup = "androidx.startup:startup-runtime:1.0.0-beta01"
        override val libraries: List<String>
            get() = listOf(startup)
    }

    object Insetter : BaseDependency() {
        private const val insetter = "dev.chrisbanes:insetter-ktx:0.3.1"
        override val libraries: List<String>
            get() = listOf(insetter)

    }
}