package com.codefactory.pokeapp.gradle.dependency

abstract class BaseDependency {
    abstract val libraries: List<String>
    operator fun invoke() = libraries
}