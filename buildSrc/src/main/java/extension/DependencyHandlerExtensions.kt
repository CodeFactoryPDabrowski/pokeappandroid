package com.codefactory.pokeapp.gradle.extension

import org.gradle.api.artifacts.dsl.DependencyHandler

fun DependencyHandler.bulkImplementation(collection: Collection<String>) {
    collection.forEach { dependency -> this.add("implementation", dependency) }
}

fun DependencyHandler.bulkApi(collection: Collection<String>) {
    collection.forEach { dependency -> this.add("api", dependency) }
}

fun DependencyHandler.bulkTestImplementation(collection: Collection<String>) {
    collection.forEach { dependency -> this.add("testImplementation", dependency) }
}

fun DependencyHandler.bulkAndroidTestImplementation(collection: Collection<String>) {
    collection.forEach { dependency -> this.add("androidTestImplementation", dependency) }
}

fun DependencyHandler.bulkDebugImplementation(collection: Collection<String>) {
    collection.forEach { dependency -> this.add("debugImplementation", dependency) }
}

fun DependencyHandler.bulkKapt(collection: Collection<String>) {
    collection.forEach { dependency -> this.add("kapt", dependency) }
}