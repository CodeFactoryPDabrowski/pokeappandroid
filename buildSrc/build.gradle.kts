import Build_gradle.Versions.kotlinVersion

plugins {
    `java-gradle-plugin`
    `kotlin-dsl`
}

gradlePlugin {
    plugins {
        val modulePluginName = "module-plugin"
        register(modulePluginName) {
            id = modulePluginName
            implementationClass = "com.codefactory.pokeapp.gradle.plugin.ModulePlugin"
        }
        val diPluginName = "di-plugin"
        register(diPluginName) {
            id = diPluginName
            implementationClass = "com.codefactory.pokeapp.gradle.plugin.DiPlugin"
        }
        val composeModulePluginName = "compose-module-plugin"
        register(composeModulePluginName) {
            id = composeModulePluginName
            implementationClass = "com.codefactory.pokeapp.gradle.plugin.ComposeModulePlugin"
        }
    }
}

repositories {
    google()
    mavenCentral()
    mavenLocal()
    jcenter()
}

allprojects {
    repositories {
        google()
        mavenCentral()
        jcenter()
    }
}

object Versions {
    const val kotlinVersion = "1.4.0"
}

dependencies {
    compileOnly(gradleApi())
    implementation("com.android.tools.build:gradle:4.0.0")
    implementation("com.getkeepsafe.dexcount:dexcount-gradle-plugin:1.0.3")
    implementation("com.google.gms:google-services:4.3.3")
    implementation(kotlin("gradle-plugin", kotlinVersion))
    implementation(kotlin("android-extensions", kotlinVersion))
}