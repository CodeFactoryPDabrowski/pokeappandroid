package com.codefactory.pokeapp.feature_pokemons.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.codefactory.domain.pokemon.entity.Pokemon
import com.codefactory.sharedui.adapter.ItemClickListener

class PokemonListAdapter(private val itemClickListener: ItemClickListener<Pokemon>) :
    PagingDataAdapter<Pokemon, PokemonViewHolder>(POKEMON_DIFF) {

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder =
        PokemonViewHolder.create(parent, itemClickListener)

    companion object {
        private val POKEMON_DIFF = object : DiffUtil.ItemCallback<Pokemon>() {
            override fun areItemsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean =
                oldItem == newItem
        }
    }
}