package com.codefactory.pokeapp.feature_pokemons

import android.os.Bundle
import android.view.View
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codefactory.domain.pokemon.entity.Pokemon
import com.codefactory.pokeapp.feature_pokemons.adapter.PokemonListAdapter
import com.codefactory.pokeapp.feature_pokemons.databinding.FragmentPokemonListBinding
import com.codefactory.sharedui.binding.viewBinding
import com.codefactory.sharedui.toolbar.ToolbarViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@ExperimentalPagingApi
@AndroidEntryPoint
class PokemonListFragment : Fragment(R.layout.fragment_pokemon_list) {

    private val binding by viewBinding(FragmentPokemonListBinding::bind)
    private val viewModel: PokemonListViewModel by viewModels()
    private val toolbarViewModel: ToolbarViewModel by activityViewModels()
    private val adapter = PokemonListAdapter { pokemon -> navigateToDetails(pokemon) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        observeList()
    }

    private fun initViews() {
        binding.pokemonList.apply {
            layoutManager =
                GridLayoutManager(
                    this@PokemonListFragment.requireContext(),
                    COLUMN_COUNT,
                    RecyclerView.VERTICAL,
                    REVERSED_LAYOUT
                )

            adapter = this@PokemonListFragment.adapter
        }
        toolbarViewModel.updateTitle(resources.getString(R.string.app_name))
    }

    private fun observeList() {
        lifecycleScope.launch {
            viewModel.pokemonListPaged.collectLatest {
                adapter.submitData(it)
            }
        }
    }

    private fun navigateToDetails(pokemon: Pokemon) {
        findNavController().navigate("app.pokeapp://details/${pokemon.id}".toUri())
    }

    companion object {
        private const val COLUMN_COUNT = 2
        private const val REVERSED_LAYOUT = false
    }
}