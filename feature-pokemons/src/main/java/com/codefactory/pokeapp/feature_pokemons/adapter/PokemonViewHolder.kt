package com.codefactory.pokeapp.feature_pokemons.adapter

import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.palette.graphics.Palette
import coil.load
import com.codefactory.domain.pokemon.entity.Pokemon
import com.codefactory.pokeapp.feature_pokemons.R
import com.codefactory.pokeapp.feature_pokemons.databinding.HolderPokemonItemBinding
import com.codefactory.sharedui.adapter.DefaultViewHolder
import com.codefactory.sharedui.adapter.ItemClickListener

class PokemonViewHolder(
    private val binding: HolderPokemonItemBinding,
    private val itemClickListener: ItemClickListener<Pokemon>
) :
    DefaultViewHolder<Pokemon>(binding) {

    override fun bind(item: Pokemon) {
        binding.pokemonName.text =
            itemView.resources.getString(R.string.pokemon_name, item.id, item.name)
        bindImageWithBackground(item)
        binding.root.setOnClickListener { itemClickListener.onClick(item) }
    }

    private fun bindImageWithBackground(item: Pokemon) {
        binding.pokemonAvatar.load(item.frontImageUrl) {
            placeholder(R.drawable.ic_pokemon_placeholder)
            target { drawable ->
                binding.pokemonAvatar.setImageDrawable(drawable)
                bindBackground(drawable)
            }
        }
    }

    private fun bindBackground(drawable: Drawable) {
        Palette.from((drawable as BitmapDrawable).bitmap).generate { palette ->
            if (palette != null) {
                val lightSwatch = palette.lightMutedSwatch
                if (lightSwatch != null) {
                    binding.pokemonCard.setCardBackgroundColor(
                        lightSwatch.rgb
                    )
                    binding.pokemonName.setTextColor(lightSwatch.titleTextColor)
                }
            }
        }
    }

    companion object {
        fun create(
            parent: ViewGroup,
            itemClickListener: ItemClickListener<Pokemon>
        ): PokemonViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.holder_pokemon_item, parent, false)
            val binding = HolderPokemonItemBinding.bind(view)

            return PokemonViewHolder(binding, itemClickListener)
        }
    }
}