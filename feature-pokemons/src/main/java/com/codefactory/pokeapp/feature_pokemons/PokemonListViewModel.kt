package com.codefactory.pokeapp.feature_pokemons

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.cachedIn
import com.codefactory.domain.pokemon.GetPokemonsUseCase

@ExperimentalPagingApi
class PokemonListViewModel @ViewModelInject constructor(
    getPokemonsUseCase: GetPokemonsUseCase
) : ViewModel() {
    val pokemonListPaged = getPokemonsUseCase(Unit).cachedIn(viewModelScope)
}