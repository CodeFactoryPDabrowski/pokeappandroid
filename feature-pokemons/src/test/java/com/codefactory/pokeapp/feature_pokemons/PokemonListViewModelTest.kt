package com.codefactory.pokeapp.feature_pokemons

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import app.cash.turbine.test
import com.codefactory.domain.pokemon.GetPokemonsUseCase
import com.codefactory.domain.pokemon.entity.Pokemon
import com.codefactory.pokeapp.sharedunittest.CoroutineTestRule
import com.codefactory.pokeapp.sharedunittest.runBlockingTest
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalPagingApi
@RunWith(MockitoJUnitRunner::class)
class PokemonListViewModelTest {
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @Mock
    private lateinit var getPokemonsUseCase: GetPokemonsUseCase

    private val pagingData = PagingData.from(listOf(Pokemon(1, "name", "url")))

    private lateinit var viewModel: PokemonListViewModel

    @Test
    fun `When load pokemons then expected success`() = coroutineRule.runBlockingTest {
        //WHEN
        whenever(getPokemonsUseCase(Unit)).thenReturn(flowOf(pagingData))
        viewModel = PokemonListViewModel(getPokemonsUseCase)
        //GIVEN
        //VERIFY
        viewModel.pokemonListPaged.test {
            //Cannot compare paging data because under the hood paging library wrapping passed value :D
            expectItem()
            expectComplete()
        }
    }

    @Test
    fun `When load pokemons then expected error`() = coroutineRule.runBlockingTest {
        val message = "cannot load page"
        val error = Exception(message)
        //WHEN
        whenever(getPokemonsUseCase(Unit)).thenReturn(flow { throw error })
        viewModel = PokemonListViewModel(getPokemonsUseCase)
        //GIVEN
        //VERIFY
        viewModel.pokemonListPaged.test {
            assertEquals(message, expectError().message)
        }
    }
}