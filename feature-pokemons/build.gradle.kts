import com.codefactory.pokeapp.gradle.dependency.Dependencies.AndroidFragment
import com.codefactory.pokeapp.gradle.dependency.Dependencies.BaseTest
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Coroutines
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Navigation
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Palette
import com.codefactory.pokeapp.gradle.extension.bulkImplementation
import com.codefactory.pokeapp.gradle.extension.bulkTestImplementation

plugins {
    id("com.android.library")
    id("module-plugin")
    id("di-plugin")
}

dependencies {
    implementation(project(":shared"))
    implementation(project(":sharedui"))
    implementation(project(":domain"))
    bulkImplementation(AndroidFragment.Impl())
    bulkImplementation(Palette())
    bulkImplementation(Navigation.Impl())
    testImplementation(project(":sharedunittest"))
    bulkTestImplementation(Coroutines.Test())
}