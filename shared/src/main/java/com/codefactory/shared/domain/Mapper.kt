package com.codefactory.shared.domain

interface Mapper<in T, out R> {
    fun transform(input: T): R
}