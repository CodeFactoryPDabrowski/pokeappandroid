import com.codefactory.pokeapp.gradle.dependency.Dependencies.Coroutines
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Paging
import com.codefactory.pokeapp.gradle.dependency.Dependencies.Utils
import com.codefactory.pokeapp.gradle.extension.bulkApi

plugins {
    id("com.android.library")
    id("module-plugin")
    id("di-plugin")
}

dependencies {
    bulkApi(Coroutines.Impl())
    bulkApi(Utils.Impl())
    bulkApi(Paging())
}